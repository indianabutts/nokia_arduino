//#include "TimerOne.h"

#define RST 3
#define CE  2
#define DC  4
#define DIN  5
#define CLK  6

#define FRAMEBUFFER_BYTES 528 
#define NAMETABLE_ENTRIES 66
#define PATTERNTABLE_ENTRIES 64
#define PATTERNTABLE_ENTRY_BYTES 8
#define OAM_ENTRIES 16
#define OAM_ENTRY_BYTES 3 

//Cant remeber this value
#define SCREEN_WIDTH 84
#define SCREEN_HEIGHT 46
#define SPRITE_LENGTH 7

#define FRAME_WIDTH 88



class Sprite{
 private:	
	byte raw_bytes[3];
 public:
  void set_xy(byte,byte);
  void set_pattern_address(byte);
  void set_flip(boolean, boolean);
  byte get_x(){
    return (raw_bytes[0]>> 1 & B01111111)>SCREEN_WIDTH?SCREEN_WIDTH:(raw_bytes[0]>> 1 & B01111111);
    };
  byte get_y(){
    return (raw_bytes[1]>> 3 & B00011111) | (raw_bytes[0]<<5 & B00100000);
    };
  byte get_pattern_address(){
    return raw_bytes[2]>> 2 & B00111111;
    };
  byte get_flip_x(){
    return raw_bytes[1]>>2 & 0x1;
    };
  byte get_flip_y(){
    return raw_bytes[1]>>1 & 0x1;
    };
  byte get_framebuffer_y(){
    return get_y()/8;
    };
};

void Sprite::set_xy(byte x, byte y){
  raw_bytes[0] = (raw_bytes[0] & 0x01) | x<<1;
  raw_bytes[0] = (raw_bytes[0] & 0xFE) | y>>7;
  raw_bytes[1] = (raw_bytes[1] & 0x07) | y<<3;
}

void Sprite::set_flip(boolean flip_x, boolean flip_y){
  raw_bytes[1] = (raw_bytes[1] & 0xF8) | flip_x<<2 | flip_y<<1;
}

void Sprite::set_pattern_address(byte pattern_address){
  raw_bytes[2] = pattern_address<<2;
}



static byte g_nametable [NAMETABLE_ENTRIES];

static byte g_patterntable [PATTERNTABLE_ENTRIES][PATTERNTABLE_ENTRY_BYTES];

static Sprite g_oam [OAM_ENTRIES];

static byte g_framebuffer [FRAMEBUFFER_BYTES];

static const byte cloud [8] = {0x30, 0x48, 0x44, 0x44, 0x48, 0x44, 0x44, 0x38};

static const byte running_man[4][8] = {{
                                  B00000000,
                                  B11000000,
                                  B01010000,
                                  B00111100,
                                  B00111110,
                                  B01111010,
                                  B10000000,
                                  B10000000
                                  },
                                  {
                                  B00000000,
                                  B01010000,
                                  B01001000,
                                  B00111000,
                                  B00111110,
                                  B11101010,
                                  B10010000,
                                  B00000000
                                    },
                                  {
                                  B00000000,
                                  B00000000,
                                  B01000000,
                                  B11111100,
                                  B11111110,
                                  B00000010,
                                  B00000000,
                                  B00000000
                                    },
                                    {
                                  B00000000,
                                  B00000000,
                                  B11101000,
                                  B00011110,
                                  B01011111,
                                  B00111101,
                                  B00000000,
                                  B00000000
                                      }
                                   };

void lcd_write_data(byte data)
{
  digitalWrite(DC, HIGH);
  digitalWrite(CE, LOW);
  shiftOut(DIN,CLK,MSBFIRST,data);
  digitalWrite(CE,HIGH);
}


void lcd_write_command(byte cmd)
{
  digitalWrite(DC, LOW); //DC pin is low for commands
  digitalWrite(CE, LOW);
  shiftOut(DIN, CLK, MSBFIRST, cmd); //transmit serial data
  digitalWrite(CE, HIGH);
}

void lcd_set_xy(byte x, byte y){
   lcd_write_command(0x80 | x);
   lcd_write_command(0x40 | y);  
}


void clear_lcd(){
  //Only the LCD and buffer should be cleared, everything else should stay the same
  for(int y=0;y<FRAMEBUFFER_BYTES;y++){
     lcd_write_data(B00000000) ;
     g_framebuffer[y]=0x00;
  }
  lcd_set_xy(0,0);
}

void blit_framebuffer(){
  lcd_set_xy(0,0);
  for (int i=0;i<FRAMEBUFFER_BYTES;i++ ){ 
      if(i%FRAME_WIDTH>=SCREEN_WIDTH) continue;
      lcd_write_data(g_framebuffer[i]);
      g_framebuffer[i]=0;
  }
 
}

void initialize_lcd(){
  pinMode(RST, OUTPUT);
  pinMode(CE, OUTPUT);
  pinMode(DC, OUTPUT);
  pinMode(DIN, OUTPUT);
  pinMode(CLK, OUTPUT);
  digitalWrite(RST, LOW);
  digitalWrite(RST, HIGH);
 
  lcd_write_command(0x21);  // LCD extended commands
  lcd_write_command(0xB8);  // set LCD Vop (contrast)
  lcd_write_command(0x04);  // set temp coefficent
  lcd_write_command(0x14);  // LCD bias mode 1:40
  lcd_write_command(0x20);  // LCD basic commands
  
  lcd_write_command(0x0C);  // LCD Normal Mode
  
  clear_lcd();

}

//This clears the tables at the beginning
void reset_tables(){
  for(int i=0;i<NAMETABLE_ENTRIES;i++){
    g_nametable[i]=0x00;
  }
  for(int i=0;i<OAM_ENTRIES;i++){
    g_oam[i].set_xy(0,0);
    g_oam[i].set_pattern_address(0x00);
  }
}


//Renders the Nametable data onto the framebuffer.
//Background would be things like floor tiles etc, that sit on the 
//8x8 grid of the screen.
void build_background(){
  for(int i=0;i<NAMETABLE_ENTRIES;i++){
      const byte patternId = g_nametable[i]>> 2 & B00111111;
      const int framebufferIndex = i<<3;
      for(int j=0;j<8;j++){
        if((framebufferIndex+j)%FRAME_WIDTH >= SCREEN_WIDTH) continue;
        g_framebuffer[framebufferIndex + j] = g_framebuffer[framebufferIndex + j] | g_patterntable[patternId][j];   
      }
  }
}

void draw_sprites(){
  /*
   * Inspired by your frame_index_top/bottom logic
   * I was thinking, why cant we instead of iterating through every framebuffer location,
   * just iterate through the OAM and only process the elements that arent pattern 0
   * And then we can just write at the correct framebuffer bytes;
  */  
  for(int oam_entry = 0; oam_entry<OAM_ENTRIES;oam_entry++){
    //Since Pattern Address 0 will be reserved as the blank pattern, skip drawing in that case
    if(g_oam[oam_entry].get_pattern_address()==0) continue;
    
    //Calculate the y Shift of the sprite by modulo of the Y with the height of the sprite.
    //This shift will let us know how to adjust the sprite and whether it needs to write in two
    //Concurrent Framebuffer Y locations
    byte yShift = g_oam[oam_entry].get_y()%8; //Use this to figure out how much shift for the two patterns overlapping
    
    //Get the frame buffer index from x and y (This formula might be reversed since im used to moving through y I can;t imagine it in my head)
    
    byte frame_index_y_top = g_oam[oam_entry].get_framebuffer_y();
      for(int fb_index=0;fb_index<8;fb_index++){
        //If the byte of a pattern is equal to 0, there is no need to write to the framebuffer so skip.
        if (g_patterntable[g_oam[oam_entry].get_pattern_address()][fb_index]==0) continue;
    
        //Need to convert from the raw X,Y Coordinates to a location in the framebuffer array
        const int framebuffer_location = g_oam[oam_entry].get_framebuffer_y()*FRAME_WIDTH+g_oam[oam_entry].get_x()+fb_index;
        
        //Dont draw the sprite if the framebuffer location is greater than the Screen Width;
        if(framebuffer_location % FRAME_WIDTH >= SCREEN_WIDTH) continue;
        
        //Check if the Sprite is flipped, if it is, index backwards through the pattern otherwise its equal to fb_index.
        const int pattern_index=g_oam[oam_entry].get_flip_x()==1?7-fb_index:fb_index;
        byte byte_pattern = g_patterntable[g_oam[oam_entry].get_pattern_address()][pattern_index];
        if(g_oam[oam_entry].get_flip_y()==1) {
          const byte reverse_lookup[16] = {
            0x0, 0x8, 0x4, 0xc, 0x2, 0xa, 0x6, 0xe,
            0x1, 0x9, 0x5, 0xd, 0x3, 0xb, 0x7, 0xf, 
           };
           byte_pattern =(reverse_lookup[byte_pattern&0b1111] << 4) | reverse_lookup[byte_pattern>>4];
        }
        g_framebuffer[framebuffer_location] |= byte_pattern<<yShift;
        
        //If the Y-Shift is greater than 0, then that means the sprite WILL overlap with the lower Y Index, so draw the correctly shifted pattern
        //In the lower FB Y Location
        if(yShift>0){
         g_framebuffer[framebuffer_location+FRAME_WIDTH] |= byte_pattern>>(8-yShift);
        }
      }    
   }
}


void setup()
{
 Serial.begin(9600);
 initialize_lcd();
 //Clear tables here
 reset_tables();
 //Putting cloud into top left nametable slot
// g_nametable[0]=0x80;
 //Adding cloud and each running man frame to the patterntable;
 for (int i=0;i<8;i++){
    g_patterntable[32][i] = cloud[i];
    g_patterntable[1][i] = running_man[0][i];
    g_patterntable[2][i] = running_man[1][i];
    g_patterntable[3][i] = running_man[2][i];
    g_patterntable[4][i] = running_man[3][i];
  }
 //Adding the running guy to the OAM
 g_oam[0].set_xy(21,22);
 g_oam[0].set_pattern_address(1);
 g_oam[0].set_flip(false,false);
 g_oam[4].set_xy(21,30);
 g_oam[4].set_pattern_address(1);
 g_oam[4].set_flip(false,true);
 
 g_oam[1].set_xy(70,0);
 g_oam[1].set_pattern_address(32);
 g_oam[2].set_xy(40,5);
 g_oam[2].set_pattern_address(32);
 g_oam[3].set_xy(15,2);
 g_oam[3].set_pattern_address(32);
 
 
 Serial.println("@@@@@Starting Run@@@@@");


}

void animate_fella(){
  byte oam_index = 0;
  if(g_oam[oam_index].get_x()+8<FRAME_WIDTH>>1){
    g_oam[oam_index].set_xy(g_oam[oam_index].get_x()+2,g_oam[oam_index].get_y());
    g_oam[4].set_xy(g_oam[4].get_x()+2,g_oam[4].get_y());
  }
  g_oam[oam_index].set_pattern_address(g_oam[oam_index].get_pattern_address()==4?1:g_oam[oam_index].get_pattern_address()+1);
  g_oam[4].set_pattern_address(g_oam[4].get_pattern_address()==4?1:g_oam[4].get_pattern_address()+1);
}

void animate_cloud(){
  byte oam_index[3] = {1,2,3};
  g_oam[oam_index[0]].set_xy(g_oam[oam_index[0]].get_x()-2<=0?FRAME_WIDTH:g_oam[oam_index[0]].get_x()-2,g_oam[oam_index[0]].get_y());
  g_oam[oam_index[1]].set_xy(g_oam[oam_index[1]].get_x()-2<=0?FRAME_WIDTH:g_oam[oam_index[1]].get_x()-2,g_oam[oam_index[1]].get_y());
  g_oam[oam_index[2]].set_xy(g_oam[oam_index[2]].get_x()-2<=0?FRAME_WIDTH:g_oam[oam_index[2]].get_x()-2,g_oam[oam_index[2]].get_y());
}
void loop()
{
//  Serial.println("#####################");
//  Serial.println("###Start of a loop###");
//  Serial.println("#####################");
  build_background();
  draw_sprites();
  blit_framebuffer();
  animate_fella();
  animate_cloud();
  delay(31);
}


